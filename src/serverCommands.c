#include "headers/PanberryMain.h"
#include "headers/PanberryServer.h"

#include <stdlib.h>
#include <string.h>


int receivedDataHandler(char *receivedData, int bytesReceived)
{
	char data[RECEIVED_DATA_HEADER_MAXSIZE];
	char *command;

	strncpy(data, receivedData, RECEIVED_DATA_HEADER_MAXSIZE - 1);
 	command = strtok(data, " ");

	if(strcmp(command, HELLO) == 0)
		return HELLO_ID;
	if(strcmp(command, EXIT) == 0)
			return EXIT_ID;
	if(strcmp(command, START) == 0)
			return START_ID;
	if(strcmp(command, ECHO) == 0)
			return ECHO_ID;
	if(strcmp(command, PANORAMA) == 0)
			return PANORAMA_ID;
	if(strcmp(command, STATUS) == 0)
			return STATUS_ID;
	if(strcmp(command, ROTATE) == 0)
			return ROTATE_ID;

	return UNRECOGNIZED_ID;
}

int helloCommandHandler(char *receivedData, int bytesReceived)
{
	//char packet[] = 
















	return EXIT_SUCCESS;
}/**/
