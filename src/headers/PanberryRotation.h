#ifndef PANBERRY_ROTATION_H
#define PANBERRY_ROTATION_H
    /* rotation's directions */
    #define CLOCKWISE 1
    #define ANTICLOCKWISE 0

	/**/
	#define MIN_ANGLE 0
    #define MAX_ANGLE 180

	#define STEP 25
	
	#define RELAX "relax"
#endif
