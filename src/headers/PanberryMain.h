#ifndef PANBERRY_MAIN_H
#define PANBERRY_MAIN_H

	#define DEFAULT_HOV 40
	#define DEFAULT_HOV_STR "40"

	/* modules */
	#define PANBERRY_SERVER "./PanberryServer"
    #define PANBERRY_PHOTO "./PanberryPhoto"
    #define PANBERRY_ROTATION "./PanberryRotation"
    #define PANBERRY_PANO "./PanberryPano"
	#define PANBERRY_INPUT "./PanberryInput"
	
	#define PATH_PANORAMAS "./panoramas"
	#define PATH_TEMP "./temp"
	#define PATH_IMAGES "./images"
	
	#define CMD_FIFO "./cmdFifo"
	#define CMD_FIFO_S  "./cmdFifoS"


	/* errors */ 
	#define SYNTAX_ERROR "syntax error"
	#define SYNTAX_ERROR_IN_PAR "syntax error in parameters"
	#define BAD_SEQUENCE_ERROR "bas sequence of commands"
	#define NO_STORAGE_ERROR "insufficient storage"
    #define SERVERS_BUSY_ERROR "server is busy"
	#define NO_CONNECTION_ERROR "no connection to device"
	#define OTHER_PROBLEM_ERROR "other problem"
	#define UNKNOWN_PROBLEM_ERROR "unknown problem"
	#define NO_IMAGES_ERROR "no images found"
	#define CANNOT_MAKE_PAN_ERROR "cannot make a panorama"
    #define APP_DOESNT_RESPOND_ERROR "application doesnt respond"

	/* errors' codes */ 
	#define NO_CONNECTION_CODE 50

	/* status codes */
	#define STATUS_READY 10
	#define STATUS_SHOOTING 11
	#define STATUS_STITCHING 12
	#define STATUS_ROTATING 13
	#define STATUS_BUSY 14
	#define STATUS_NO_CAMERA 15
	#define STATUS_EMPTY 16
	#define STATUS_ERR 17
    #define STATUS_TERMINATING 18

	/* diodes */
	#define WHITE_DIODE 1000
	#define YELLOW_DIODE 10001
	#define RED_DIODE 1002
	#define GREEN_DIODE 1003

	/* actions */
    #define NO_ACTION 0
    #define ACTION_START 1
    #define ACTION_ROTATE 2
    #define ACTION_STATUS 3
    #define ACTION_TERMINATE 4
	#define ACTION_ROTATE_L 5
	#define ACTION_ROTATE_R 6

	/* maxsizes */ 
	#define STATUS_MAXSIZE  100
	#define ACTION_MAXSIZE  10

	

#endif
