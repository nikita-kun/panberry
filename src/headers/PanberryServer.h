#ifndef PANBERRY_SERVER_H
#define PANBERRY_SERVER_H

	#define DEFAULT_PORT "50000"
	#define DEFAULT_ADR "127.0.0.1"

	
	#define BACKLOG 10

	/* maxsizes */

    #define COMMAND_MAXSIZE 20

	#define RECEIVED_DATA_MAXSIZE 262144100 // 250 Mb (data) + 100 bytes (header)
	#define RECEIVED_DATA_HEADER_MAXSIZE 100

	/* timeout */
	#define TIMEOUT_SECS 60
	#define TIMEOUT_USECS 0

	/* commands */
	#define HELLO "HELLO"
	#define EXIT "EXIT"
	#define START "START"
	#define ECHO "ECHO"
	#define PANORAMA "PANORAMA"
 	#define ROTATE "ROTATE"
	#define STATUS "STATUS"

	/* id */
	#define UNRECOGNIZED_ID -1
	#define HELLO_ID 0
	#define EXIT_ID	1
	#define START_ID 2
	#define ECHO_ID 3
	#define PANORAMA_ID 4
 	#define ROTATE_ID 5
	#define STATUS_ID 6

	int receivedDataHandler(char *receivedData, int bytesReceived);

	int helloCommandHandler(char *receivedData, int bytesReceived);
	int exitCommandHandler(char *receivedData, int bytesReceived);
	int	startCommandHandler(char *receivedData, int bytesReceived);	
	int echoCommandHandler(char *receivedData, int bytesReceived);
	int panoramaCommandHandler(char *receivedData, int bytesReceived);
	int rotateCommandHandler(char *receivedData, int bytesReceived);
	int statusCommandHandler(char *receivedData, int bytesReceived);	
	int unrecognizedCommand(char *receivedData, int bytesReceived);

	int checkStatus(char *statusString, int size);

	void awaitClient(int socketFd, int seconds, int useconds);		
	int encodePanorama(int fd);

#endif
