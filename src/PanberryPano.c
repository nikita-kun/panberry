#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "headers/PanberryMain.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    
    char cmd[124];
    if(argc != 1) {
        fprintf(stderr, "usage ./PanberryPano");
        return EXIT_FAILURE;
    }

    sprintf(cmd, "rm -f %s/*.tif", PATH_TEMP);
    system(cmd);

    sprintf(cmd, "rm -f %s/*.pto", PATH_TEMP);
    system(cmd);
    


    //Сгенерировать .pto файл (файл панорамы) из списка фотографий вида temp/*.jpg
    //Сохранить под именем temp/tmp.pto
    sprintf(cmd, "match-n-shift %s/*.jpg --output %s/step1.pto", PATH_IMAGES, PATH_TEMP);
    system(cmd);

    // линейное сопоставление кадров (а не каждый с каждым), параметры поиска снижены в угоду
    // роизводительности
    sprintf(cmd, "cpfind -o %s/step2.pto --fullscale --linearmatch %s/step1.pto", PATH_TEMP, PATH_TEMP);//; --sieve1size 25 --sieve2size 25 --kdtreesteps 100",
            
    system(cmd);

    //удаление неверных контрольных точек (проверка всех пар кадров на наличие ошибок)
    //результирующий файл сохраняем в ./temp/tmp.pto
    sprintf(cmd, "cpclean -p -o %s/step3.pto %s/step2.pto", PATH_TEMP, PATH_TEMP);
    system(cmd);

    //оптимизация позиций кадров
    sprintf(cmd, "autooptimiser -l -p -o %s/step4.pto %s/step3.pto", PATH_TEMP, PATH_TEMP);
    system(cmd);

    //рендеринг, искажение, конвертация в tif
    sprintf(cmd, "nona -m TIFF_m -o %s/tmp %s/step4.pto", PATH_TEMP, PATH_TEMP);
    system(cmd);

    time_t rawtime;
    time(&rawtime);
    struct tm *timeinfo = localtime(&rawtime);

    char name[100];
    strftime(name, 100,"pan%d-%m-%Y-%H-%M.tif", timeinfo);
puts(name);
    //Склейка кадров .tif в один
    sprintf(cmd, "enblend -w  -o %s/%s %s/tmp*.tif --no-optimize", PATH_PANORAMAS, name, PATH_TEMP);
    system(cmd);

    char panorama[100];
    sprintf(panorama, "%s/%s", PATH_PANORAMAS, name);
    //stat(cmd, &file);
    
    sprintf(cmd, "chmod 644 %s", panorama);
    system(cmd);
    
     sprintf(cmd, "rm -f %s/*.jpg", PATH_IMAGES);
    system(cmd);
    
     sprintf(cmd, "rm -f %s/*", PATH_TEMP);
    system(cmd);

   

    fprintf(stderr, "path %s\n", panorama);
    if(access(panorama, F_OK) != 0)
    {   
        perror("access");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
   
}
