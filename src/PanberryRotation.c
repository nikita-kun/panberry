#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "headers/PanberryMain.h"
#include "headers/PanberryRotation.h"

int main(int argc, char *argv[])
{
    char systemCall[128];
    if (argc < 2 || argc > 3){
		printf("usage ./PanberryRotation [angle 0-180] [relax]\n");
		return EXIT_SUCCESS;
    }   

    // destination angle here 0
    int angle = (int) strtol(argv[1], NULL, 10);
    if (angle < MIN_ANGLE)
		angle = MIN_ANGLE;
    if (angle > MAX_ANGLE)
		angle = MAX_ANGLE;
    
    //if (angle == 0)
	//angleTransposed = 1.0;
   // sprintf(systemCall, "gpio pwm 1 %5.1f", angleTransposed);
    //fprintf(stderr, "angle - %d; value = %f \n", angle, angleTransposed);

	float angleTransposed = angle * 5.0 + 100;
	sprintf(systemCall, "gpio pwm 1 %5.1f", angleTransposed);
    fprintf(stderr, "angle - %d; value = %f \n", angle, angleTransposed);
    system(systemCall);
	 
    if (argc == 3 && strcmp(argv[2], RELAX) == 0){
    	usleep(400000);
    	system("gpio pwm 1 0");
    	fprintf(stderr, "%sed\n", RELAX);
    }
    return EXIT_SUCCESS;
}
