#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#include <string.h>
#include <gphoto2/gphoto2-camera.h>
#include <gphoto2/gphoto2.h>

#include <fcntl.h>  

#include "headers/PanberryMain.h"
#include "headers/PanberryServer.h"

int main(int argc, char *argv[])
{	
	int returnValue = EXIT_SUCCESS;

    if(argc <= 1)
    {
        fprintf(stderr, "usage ./PanberyPhoto [destination]\n");
        return EXIT_FAILURE;
    } else {
        //fprintf(stderr, "PanberyPhoto Init");
    }

	Camera *camera;
	GPContext *context;
	gp_camera_new(&camera);
 	context = gp_context_new();

	if (gp_camera_init(camera, context) < GP_OK) 
	{
	 	fprintf(stderr, "No camera auto detected.\n");
        gp_camera_unref(camera);
    	gp_context_unref(context);
	 	return EXIT_FAILURE;
	}

	int fd;
	CameraFile *file;
	CameraFilePath cameraFilePath;

	// take a shot
	if(gp_camera_capture(camera, GP_CAPTURE_IMAGE, &cameraFilePath, context) == 1)
 	{
		fprintf(stderr, "An error occured during taking a shot\n");
        gp_camera_unref(camera);
        gp_context_unref(context);
	 	return EXIT_FAILURE;
 	}

	fd = open(argv[1], O_CREAT | O_WRONLY);
	// create new CameraFile object from a file descriptor
	if (gp_file_new_from_fd(&file, fd) == 1) 
	{
    	fprintf(stderr, "An error occured\n");
        gp_camera_unref(camera);
        gp_context_unref(context);
	 	return EXIT_FAILURE;
	}

    // copy picture from camera
	if (gp_camera_file_get(camera, cameraFilePath.folder, cameraFilePath.name, GP_FILE_TYPE_NORMAL, file, context) == 1) 
	{
		fprintf(stderr, "An error occured during copying from camera\n");
        gp_camera_unref(camera);
    	gp_context_unref(context);
	 	return EXIT_FAILURE;
 	}


	// remove picture from camera memory
	if (gp_camera_file_delete(camera, cameraFilePath.folder, cameraFilePath.name, context) == 1)
	{
		fprintf(stderr, "An error occured during removing from camera\n");
		gp_camera_unref(camera);
    	gp_context_unref(context);
	 	return EXIT_FAILURE;
	}

	// free CameraFile object
	gp_file_free(file);
    /*
	// Code from here waits for camera to complete everything.
	int waittime = 100;
	CameraEventType type;
	void *data;
	int retval;
	while(1) 
	{
		retval = gp_camera_wait_for_event(camera, waittime, &type, &data, context);
		if(type == GP_EVENT_TIMEOUT) 
		{
			fprintf(stderr,"// time run out");
			returnValue = EXIT_FAILURE;
			break;
		}
		else if (type == GP_EVENT_CAPTURE_COMPLETE) 
		{
			fprintf(stderr,"//Capture completed");
	   		break;
		}
	   else if (type != GP_EVENT_UNKNOWN) 
		{
	        fprintf(stderr,"//Unexpected event received from camera");
			returnValue = EXIT_FAILURE;
			break;
	   }
 	}
    */
    struct stat st;
    stat(argv[1], &st);
    int size = st.st_size;
    fprintf(stderr,"size=%d\n",size);
    if(access(argv[1], F_OK) != 0 || size <= 0)
    {   
        fprintf(stderr,"Bad camera\n");
        perror("access");
        char cmd[100];
        sprintf(cmd, "rm %s",argv[1]);
        system(cmd);
        returnValue = EXIT_FAILURE;
    }
	// close camera
    //gp_camera_free(camera);
    gp_camera_unref (camera);
	//gp_camera_unref(camera);
    gp_context_unref(context);

    
   // fprintf(stderr, "return code %d", returnValue);
    

	return returnValue;
}
